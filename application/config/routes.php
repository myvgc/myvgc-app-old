<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

// ACCOUNT
$route['account/activate/(:any)']       = 'account/activate/$1';
$route['account/forgot-password']       = 'account/forgot_password';
$route['account/login']                 = 'account/login';
$route['account/logout']                = 'account/logout';
$route['account/reset-password/(:any)'] = 'account/reset_password/$1';
$route['account/settings']              = 'account/settings';
$route['account/sign-up']               = 'account/register';


// BROWSE
$route['browse/items']                  = 'browse/item_list';
$route['browse/users']                  = 'browse/user_list';


// ITEM
$route['i/(:any)']                      = 'browse/item/$1';
$route['item/(:any)']                   = 'browse/item/$1';


// USER - LOGGED IN
$route['collection/dashboard/(:any)']   = '';
$route['collection/view/(:any)']        = '';


// USER - PUBLIC
$route['u/(:any)']                      = 'browse/user/$1';
$route['user/(:any)']                   = 'browse/user/$1';


$route['default_controller']            = 'account';
$route['404_override']                  = '';
$route['translate_uri_dashes']          = FALSE;
