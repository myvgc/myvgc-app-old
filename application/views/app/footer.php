                    <div class="clearfix"></div>
                </div>
                
                <footer>
                    <div class="pull-left">
                        &copy; 2016
                        <? if (date('Y') > 2016) { echo ' - ' . date('Y'); } ?>
                        <a href="/myvgc/about">myVGC</a>
                        &nbsp;&bull;&nbsp;
                        <a href="/myvgc/privacy-policy">Privacy Policy</a>
                        &nbsp;&bull;&nbsp;
                        <a href="/myvgc/terms-of-service">Terms of Service</a>
                        &nbsp;&bull;&nbsp;
                        <a href="/myvgc/credits">Credits</a>
                    </div>
                    <div class="pull-right">
                        <? echo RELEASE_VERSION; ?>
                        &nbsp;&bull;&nbsp;
                        {elapsed_time}
                    </div>
                    <div class="clearfix"></div>
                </footer>
            </div>
        </div>
        <?php
            if ( ! empty($media_array)) {
                if ( ! empty($media_array['css'])) {
                    foreach ($media_array['css'] AS $file_path) {
                        echo '<link href="' . $file_path . '" rel="stylesheet">' . PHP_EOL;
                    }
                }
                if ( ! empty($media_array['js'])) {
                    foreach ($media_array['js'] AS $file_path) {
                        echo '<script type="text/javascript" src="' . $file_path . '"></script>' . PHP_EOL;
                    }
                }
            }
        ?>
    </body>
</html>