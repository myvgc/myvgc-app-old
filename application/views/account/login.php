<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
        
                <form action="">
                    <div class="form-group">
                        <input type="email" class="form-control input-lg" name="login_string" placeholder="Email" value="">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" name="login_pass" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <button id="login_submit" type="submit" class="btn btn-primary btn-lg pull-left"><i class="fa fa-fw fa-sign-in"></i> Login</button>
                        <a href="/account/forgot-password" class="btn btn-link btn-lg pull-right">Forgot password?</a>
                        <div class="clearfix"></div>
                    </div>
                </form>
        
            </div>
        </div>
    </div>
</div>