<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
        <div class="x_panel">
            <div class="x_content">

<form action="">
    <div class="alert alert-info">
        <strong>These settings aren't for life!</strong><br>They can be changed at any time after you login to your account.
    </div>
    
    <fieldset>
        <h4>Username <i class="fa fa-fw fa-info-circle myvgc-help-icon" data-toggle="popover" title="Username Rules" data-content="Username must be unique (case insensitive). Can contain letters, numbers, hyphens and underscores. Minimum 4 characters. Maximum 50 characters." data-placement="right" data-trigger="hover click" data-viewport="body"></i></h4>
        <div class="form-group">
            <input type="text" class="form-control input-lg" name="username" placeholder="Username" maxlength="50" value="">
        </div>
    </fieldset>
    <fieldset>
        <h4>Email <i class="fa fa-fw fa-info-circle myvgc-help-icon" data-toggle="popover" title="Email Rules" data-content="Must be valid. A verification email will be sent to this address in order to activate your account." data-placement="right" data-trigger="hover click" data-viewport="body"></i></h4>
        <div class="form-group">
            <input type="email" class="form-control input-lg" name="email" placeholder="Email" value="">
        </div>
        <div class="form-group">
            <input type="email" class="form-control input-lg" name="email_confirm" placeholder="Confirm Email" value="">
        </div>
    </fieldset>
    <fieldset>
        <h4>Password <i class="fa fa-fw fa-info-circle myvgc-help-icon" data-toggle="popover" title="Password Rules" data-content="Minimum 8 characters." data-placement="right" data-trigger="hover click" data-viewport="body"></i></h4>
        <div class="form-group">
            <input type="password" class="form-control input-lg" name="password" placeholder="Password">
        </div>
        <div class="form-group">
            <input type="password" class="form-control input-lg" name="password_confirm" placeholder="Confirm Password">
        </div>
    </fieldset>
    <fieldset>
        <h4>Currency <i class="fa fa-fw fa-info-circle myvgc-help-icon" data-toggle="popover" title="Currency" data-content="This will be your default currency. Values in other currencies will automatically be converted (based on the current exchange rate) into your selected currency." data-placement="right" data-trigger="hover click" data-viewport="body"></i></h4>
        <div class="form-group">
            <select class="form-control input-lg" name="currency">
                <option value=""></option>
                <!--
                <?
                    if ( ! empty($currency_array)):
                        foreach ($currency_array AS $row):
                ?>
                    <option value="<?=$row['CurrencyID']?>"><?=$row['Name']?> (<?=$row['Code']?>)</option>
                <?
                        endforeach;
                    endif;
                ?>
                -->
            </select>
        </div>
    </fieldset>
    <fieldset>
        <h4>Your Profile <i class="fa fa-fw fa-info-circle myvgc-help-icon" data-toggle="popover" title="Your Profile" data-content="A public profile allows anyone to view your data (if you want to show off or share with your friends). A private profile allows only you to view your data." data-placement="right" data-trigger="hover click" data-viewport="body"></i></h4>
        <div class="form-group">
            <div class="btn-group btn-group-justified" data-toggle="buttons">
                <label class="btn btn-default active">
                    <input type="radio" name="profile_view" id="profile_public" value="public" autocomplete="off" checked> <i class="fa fa-fw fa-unlock"></i> Public
                </label>
                <label class="btn btn-default">
                    <input type="radio" name="profile_view" id="profile_private" value="private" autocomplete="off"> <i class="fa fa-fw fa-lock"></i> Private
                </label>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <h4>Are you a human?</h4>
        <div class="form-group">
            <div class="g-recaptcha" data-sitekey="6LcgsCUTAAAAAKOWDt2wNw_VEk3PtYuE83o03vrV"></div>
        </div>
    </fieldset>
    <fieldset>
        <h4>&nbsp;</h4>
        <div class="form-group">
            <button id="register_button" type="submit" class="btn btn-primary btn-lg pull-left">Sign Up</button>
            <div class="clearfix"></div>
        </div>
    </fieldset>
</form>


            </div>
        </div>
    </div>
</div>