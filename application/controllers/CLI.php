<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CLI extends CI_Controller {

    /**
     *  Construction
     */
    public function __construct() {
        parent::__construct();
        
        if ( ! $this->input->is_cli_request()) {
            echo 'Only CLI.';
            exit();
        }
    }


    /**
     *  Pull a currency update from the API and store locally.
     *
     *  @param string   $start_date     The start date (inclusive) to start updating. Defaults to today.
     *  @param string   $end_date       The end date (inclusive) to stop updating. Defaults to start date.
     */
    public function currency_update_api($start_date = '', $end_date = '') {
        // Default to today's date
        $historical_start_ts    = time();
        $historical_end_ts      = $historical_start_ts;
        
        // Does the input appear to be a valid start date?
        if ( ! empty($start_date) && strtotime($start_date) !== FALSE) {
            $historical_start_ts    = strtotime($start_date);
            $historical_end_ts      = $historical_start_ts;
        }
        
        // Does the input appear to be a valid end date?
        if ( ! empty($end_date) && strtotime($end_date) !== FALSE && $historical_start_ts < strtotime($end_date)) {
            $historical_end_ts = strtotime($end_date);
        }
        
        echo 'Input start date: ' . $start_date . PHP_EOL;
        echo 'Using start date: ' . date('Y-m-d', $historical_start_ts) . PHP_EOL;
        echo 'Input end date: ' . $end_date . PHP_EOL;
        echo 'Using end date: ' . date('Y-m-d', $historical_end_ts) . PHP_EOL;
        
        // Grab the config file
        $this->load->config('currency');
        
        // Loop through each day
        $current_ts = $historical_start_ts;
        while ($current_ts <= $historical_end_ts) {
            echo 'Starting day ' . date('Y-m-d', $current_ts) . PHP_EOL;
            
            // Build date dir path
            $dir_path = $this->config->item('currency_dir_base_path');
            $dir_path .= date($this->config->item('currency_dir_date_path'), $current_ts);
            
            echo 'Dir path: ' . $dir_path . PHP_EOL;
            
            // Does the historical folder exist?
            if ( ! is_dir($dir_path)) {
                echo 'Dir does not exist, attempting to create...' . PHP_EOL;
                mkdir($dir_path, 0777, TRUE);
            }
            
            // Build the file path
            $file_path = date($this->config->item('currency_file_date_path'), $current_ts);
            $file_path .= $this->config->item('currency_file_extension_api');
           
            echo 'File path: ' . $file_path . PHP_EOL;
           
            // Check if this date already exists
            if ( ! file_exists($dir_path . $file_path)) {
                echo 'File does not exist, attempting to get from API...' . PHP_EOL;

                $api_path = sprintf($this->config->item('currency_api_host'), date('Y-m-d', $current_ts), $this->config->item('currency_api_key'));
    
                echo 'API path: ' . $api_path . PHP_EOL;
    
    	        $api_data = file_get_contents($api_path);
    
                // Did data come back from the API call?
                if ( ! empty($api_data)) {
                    echo 'Got API data. Checking data...' . PHP_EOL;
                    
                    $json_data = json_decode($api_data, TRUE);
                    
                    if (empty($json_data)) {
                        echo 'FAILED to json decode data.' . PHP_EOL;
                        exit(EXIT_ERROR);
                    }
                    
                    if (isset($json_data['error']) && $json_data['error'] == 'true') {
                        echo 'FAILED API data had error.' . PHP_EOL;
                        exit(EXIT_ERROR);
                    }
                    
                    echo 'Got API data. Attempting to write file contents...' . PHP_EOL;
                    
                    if (file_put_contents($dir_path . $file_path, $api_data) !== FALSE) {
        				echo 'Wrote file to contents. Attempting to chmod...' . PHP_EOL;
        				chmod($dir_path . $file_path, 0777);
        			}
        			else {
        				echo 'FAILED to write contents to file.' . PHP_EOL;
        				exit(EXIT_ERROR);
        			}
                }
                else {
                    echo 'FAILED to get API data.' . PHP_EOL;
                    exit(EXIT_ERROR);
                }
            }
            else {
                echo 'File already exists. Nothing left to do.' . PHP_EOL;
            }
         
            // Advance to the next day
            $current_ts = strtotime('+1 day', $current_ts);
            
            // 100ms
            usleep(100000);
        }
       
        echo 'Done!' . PHP_EOL;
        exit(EXIT_SUCCESS);
    }
    
    
    /**
     *  Grab a local currency file and update the database.
     *
     *  @param string   $start_date     The start date (inclusive) to start updating. Defaults to today.
     *  @param string   $end_date       The end date (inclusive) to stop updating. Defaults to start date.
     */
    public function currency_update_database($start_date = '', $end_date = '') {
        // Default to today's date
        $historical_start_ts    = time();
        $historical_end_ts      = $historical_start_ts;
        
        // Does the input appear to be a valid start date?
        if ( ! empty($start_date) && strtotime($start_date) !== FALSE) {
            $historical_start_ts    = strtotime($start_date);
            $historical_end_ts      = $historical_start_ts;
        }
        
        // Does the input appear to be a valid end date?
        if ( ! empty($end_date) && strtotime($end_date) !== FALSE && $historical_start_ts < strtotime($end_date)) {
            $historical_end_ts = strtotime($end_date);
        }
        
        echo 'Input start date: ' . $start_date . PHP_EOL;
        echo 'Using start date: ' . date('Y-m-d', $historical_start_ts) . PHP_EOL;
        echo 'Input end date: ' . $end_date . PHP_EOL;
        echo 'Using end date: ' . date('Y-m-d', $historical_end_ts) . PHP_EOL;
        
        // Grab the config file
        $this->load->config('currency');
        
        // Loop through each day
        $first_day = TRUE;
        $current_ts = $historical_start_ts;
        while ($current_ts <= $historical_end_ts) {
            
            // Advance to the next day
            if ( ! $first_day) {
                $current_ts = strtotime('+1 day', $current_ts);
                if ($current_ts > $historical_end_ts) {
                    break;
                }
            }
            $first_day = FALSE;
            
            echo 'Starting day ' . date('Y-m-d', $current_ts) . PHP_EOL;
        
            // Build date dir path
            $dir_path = $this->config->item('currency_dir_base_path');
            $dir_path .= date($this->config->item('currency_dir_date_path'), $current_ts);
            
            echo 'Dir path: ' . $dir_path . PHP_EOL;
            
            // Does the historical folder exist?
            if ( ! is_dir($dir_path)) {
                echo 'Dir does not exist, attempting to create...' . PHP_EOL;
                mkdir($dir_path, 0777, TRUE);
            }
            
            // Build the API file path
            $file_path = date($this->config->item('currency_file_date_path'), $current_ts);
            $file_path .= $this->config->item('currency_file_extension_api');
           
            echo 'API File path: ' . $file_path . PHP_EOL;
           
            // Check if this date already exists
            if ( ! file_exists($dir_path . $file_path)) {
                echo 'API file does not exist. Skipping.' . PHP_EOL;
                continue;
            }
            
            // Build the import file path
            $import_file_path = date($this->config->item('currency_file_date_path'), $current_ts);
            $import_file_path .= $this->config->item('currency_file_extension_db');
            
            echo 'Import File path: ' . $import_file_path . PHP_EOL;
            
            // Has this file been processed by the db?
            if (file_exists($dir_path . $import_file_path)) {
                echo 'Import file exists. Skipping.' . PHP_EOL;
                continue;
            }
            
            // Get current currencies
            $currency_array = array();
            $this->db->select('CurrencyID,Code');
            $query = $this->db->get('vgc_currencies');
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() AS $row) {
                    $currency_array[$row['Code']] = $row['CurrencyID'];
                }
            }
            
            // Parse the API json file
            $json_array = json_decode(file_get_contents($dir_path . $file_path), TRUE);
            if (empty($json_array) OR empty($json_array['rates'])) {
                echo 'FAILED to get json array.' . PHP_EOL;
                exit(EXIT_ERROR);
            }
            
            // Check if we need to insert any new currencies
            echo 'Checking for currencies to insert...' . PHP_EOL;
            $insert_array = array();
            foreach ($json_array['rates'] AS $code => $value) {
                if ( ! isset($currency_array[$code])) {
                    $insert_array[] = array(
                        'Code'  => $code,
                        'Value' => $value
                    );
                }
            }
            
            // Do we need to insert any
            if ( ! empty($insert_array)) {
                echo 'Inserting new currencies...' . PHP_EOL;
                $result = $this->db->insert_batch('vgc_currencies', $insert_array);
                
                if ($result === FALSE) {
                    echo 'FAILED to insert currencies.' . PHP_EOL;
                    exit(EXIT_ERROR);
                }
                echo $result . ' rows inserted.' . PHP_EOL;
                
                // Get current currencies (again - to get new IDs)
                $currency_array = array();
                $this->db->select('CurrencyID,Code');
                $query = $this->db->get('vgc_currencies');
                if ($query->num_rows() > 0) {
                    foreach ($query->result_array() AS $row) {
                        $currency_array[$row['Code']] = $row['CurrencyID'];
                    }
                }
            }
            
            // Populate the daily insert array
            echo 'Building daily currency insert...' . PHP_EOL;
            $insert_array = array();
            foreach ($json_array['rates'] AS $code => $value) {
                if ( ! isset($currency_array[$code])) {
                    echo 'Did not find currency: ' . $code . PHP_EOL;
                    exit(EXIT_ERROR);
                }
                $insert_array[] = array(
                    'CurrencyID'    => $currency_array[$code],
                    'Value'         => $value,
                    'Date'          => date('Y-m-d', $current_ts)
                );
            }
    
            $this->db->trans_start();
            
            echo 'Inserting currency history...' . PHP_EOL;
            $result = $this->db->insert_batch('vgc_currency_history', $insert_array);
            
            if ($result === FALSE) {
                echo 'FAILED to insert currency history.' . PHP_EOL;
                exit(EXIT_ERROR);
            }
            echo $result . ' rows inserted.' . PHP_EOL;
            
            // If we're on today then update the main table too
            if (date('Y-m-d') == date('Y-m-d', $current_ts)) {
                echo 'Updating the current day...' . PHP_EOL;
                foreach ($insert_array AS $index => $row) {
                    unset($insert_array[$index]['Date']);
                }
                $result = $this->db->update_batch('vgc_currencies', $insert_array, 'CurrencyID');
                
                if ($result === FALSE) {
                    echo 'FAILED to update currencies.' . PHP_EOL;
                    exit(EXIT_ERROR);
                }
                echo $result . ' rows updated.' . PHP_EOL;
            }
            
            // Create a file to note this date has been imported
            if (file_put_contents($dir_path . $import_file_path, time()) !== FALSE) {
    			echo 'Wrote file to contents. Attempting to chmod...' . PHP_EOL;
    			chmod($dir_path . $import_file_path, 0777);
    		}
    		else {
    			echo 'FAILED to write contents to file.' . PHP_EOL;
    			exit(EXIT_ERROR);
    		}
    
            $this->db->trans_complete();
            
            // 100ms
            usleep(100000);
        }

        echo 'Done!' . PHP_EOL;
        exit(EXIT_SUCCESS);
    }



    /**
     *  Pull a PriceCharting update from the download link and store locally.
     *
     *  (Currently this is no support to request historical data.)
     */
    public function pricecharting_update_download() {
        $current_ts     = time();
 
        echo 'Using date: ' . date('Y-m-d', $current_ts) . PHP_EOL;

        // Grab the config file
        $this->load->config('pricecharting');
        
        // Build the current dir path
        $dir_path = $this->config->item('pricecharting_dir_base_path');
        $dir_path .= date($this->config->item('pricecharting_dir_date_path'), $current_ts);
        
        echo 'Dir path: ' . $dir_path . PHP_EOL;
        
        // Does the historical folder exist?
        if ( ! is_dir($dir_path)) {
            echo 'Dir does not exist, attempting to create...' . PHP_EOL;
            mkdir($dir_path, 0777, TRUE);
        }
            
        // Build the current file path
        $file_path = date($this->config->item('pricecharting_file_date_path'), $current_ts);
        $file_path .= $this->config->item('pricecharting_file_extension_download');
       
        echo 'File path: ' . $file_path . PHP_EOL;
        
        // Check if this date already exists
        if ( ! file_exists($dir_path . $file_path)) {
            echo 'File does not exist, attempting to get from download link...' . PHP_EOL;

            $api_path = sprintf($this->config->item('pricecharting_download_host'), $this->config->item('pricecharting_api_key'));

            echo 'Download path: ' . $api_path . PHP_EOL;

	        $api_data = file_get_contents($api_path);

            // Did data come back from the API call?
            if ( ! empty($api_data)) {
                echo 'Got download data. Attempting to write file contents...' . PHP_EOL;
                
                if (file_put_contents($dir_path . $file_path, $api_data) !== FALSE) {
    				echo 'Wrote file to contents. Attempting to chmod...' . PHP_EOL;
    				chmod($dir_path . $file_path, 0777);
    				
    				if (file_exists($yes_dir_path . $yes_file_path)) {
    			        echo 'Chmod\'d file. Checking against yesterday file...' . PHP_EOL;
    			        
    			        if (filesize($dir_path . $file_path) == filesize($yes_dir_path . $yes_file_path)
    			            && sha1_file($dir_path . $file_path) == sha1_file($yes_dir_path . $yes_file_path)) {
    			            echo 'Current is same as yesterday. Deleting...' . PHP_EOL;
    			            if (unlink($dir_path . $file_path) === FALSE) {
    			                echo 'FAILED to delete current file.' . PHP_EOL;
    			                exit(EXIT_ERROR);
    			            }
    			        }
    				}
    			}
    			else {
    				echo 'FAILED to write contents to file.' . PHP_EOL;
    				exit(EXIT_ERROR);
    			}
            }
            else {
                echo 'FAILED to get download data.' . PHP_EOL;
                exit(EXIT_ERROR);
            }
        }
        else {
            echo 'File already exists. Nothing left to do.' . PHP_EOL;
        }
       
        echo 'Done!' . PHP_EOL;
        exit(EXIT_SUCCESS);
    }
    
    

}
