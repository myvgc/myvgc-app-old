<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Browse extends VGC_Controller {


    public function item($item_id = 0, $item_slug = '') {
        $this->page_title = 'Item Name';
        $this->render('browse/item_404');
    }


    public function item_list() {
        $this->page_title = 'Browse Items';
        $this->render('browse/item_list');
    }

    
    public function user($username = '') {
        $this->page_title = $username;
        $this->render('browse/user_404');
    }


    public function user_list() {
        $this->page_title = 'Browse Users';
        $this->render('browse/user_list');
    }


}
