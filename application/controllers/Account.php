<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends VGC_Controller {

	/**
	 *
	 */
	public function login() {
	    $this->page_title = 'Login';
		$this->render('account/login');
	}
	
	
	/**
	 *
	 */
	public function register() {
	    $this->page_title = 'Sign up';
		$this->render('account/register');
	}
}
