<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VGC_Controller extends CI_Controller {

    public $page_title              = 'set the page title';

    protected $cdn_host             = '';

    private $_media_include_array   = array('footer' => array(), 'header' => array());


    /**
     *  Begin the construction!
     */
    public function __construct() {
        parent::__construct();

        $this->cdn_host = config_item('cdn_host');
    }


    /**
     *  Include a media file into the header or footer view.
     *
     *  @param string   $media_host         The host of the media file (CDN or MODULE).
     *  @param string   $file_path          The absolute path to the media file.
     *  @param string   $include_position   Whether to include in TOP/HEADER or BOTTOM/FOOTER view.
     */
    public function include_media_file($media_host = '', $file_path = '', $include_position = 'top') {
        
        // Determine position key
        $position_key = 'header';
        if (in_array(strtolower($include_position), array('bottom', 'footer'))) {
            $position_key = 'footer';
        }
        
        // Determine file type
        $file_type = 'unknown';
        if (substr($file_path, -3) == '.js') {
            $file_type = 'js';
        }
        else if (substr($file_path, -4) == '.css') {
            $file_type = 'css';
        }
        
        // Construct the full path to the media file
        $full_path = $file_path;
        if (strtolower($media_host) == 'cdn') {
            $full_path = '//' . $this->cdn_host . $file_path;
        }
        else if (strtolower($media_host) == 'module') {
            if (ENVIRONMENT == ENV_DEV) {
                $full_path = '/media' . $file_path;
            }
            else {
                $full_path = '//' . $this->cdn_host . '/releases/' . RELEASE_VERSION . $file_path;
            }
        }
        
        // Add to media include array
        $this->_media_include_array[$position_key][$file_type][$file_path] = $full_path;
    }


    /**
     *  Render a view within the myVGC template.
     *
     *  @param string   $view_path          The path to the view file.
     *  @param array    $view_data          The data array to pass to the view file.
     */
    public function render($view_path = '', $view_data = array()) {
        
        // Pre-render header
        $this->_render_app_header();
        
        // Render the given view
        $this->load->view($view_path, $view_data);
        
        // Pre-render footer
        $this->_render_app_footer();
    }


    /**
     *  Render the myVGC app footer.
     */
    private function _render_app_footer($view_data = array()) {
        
        // Include the needed media files
        $this->include_media_file('ext', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js', 'bottom');
        $this->include_media_file('ext', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', 'bottom');
        $this->include_media_file('cdn', '/app/third-party/gentelella/build/js/custom.min.js', 'bottom');
        
        $view_data['media_array']   = $this->_media_include_array['footer'];
        
        $this->load->view('app/footer', $view_data);
    }


    /**
     *  Render the myVGC app header.
     */
    private function _render_app_header($view_data = array()) {
        
        // Include needed media files
        $this->include_media_file('ext', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', 'top');
        $this->include_media_file('ext', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', 'top');
        $this->include_media_file('cdn', '/app/third-party/gentelella/build/css/custom.min.css', 'top');
        $this->include_media_file('module', '/css/myvgc-base.css', 'top');

        $view_data['media_array']   = $this->_media_include_array['header'];
        $view_data['page_title']    = $this->page_title;
        
        $this->load->view('app/header', $view_data);
    }


}
